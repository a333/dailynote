﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DailyNote
{
    enum LoggerState
    {
        Debug=0,Info=1,Warn=2,Error=3
    }
    static class Logger
    {
        private static bool LogToOutput = false;
        public static LoggerState LoggerState = LoggerState.Debug;
        //public static void CatchUnhadledExceptions()
        //{
        //    AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
        //    AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        //    TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
        //    System.Windows.Forms.Application.ThreadException += Application_ThreadException;
        //}

        //static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        //{
        //    Log.Error("Unhandled ThreadException at application level", e.Exception);
        //}

        //static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        //{
        //    Log.Error("Unhandled UnobservedTaskException", e.Exception);
        //    e.SetObserved();
        //}

        //static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        //{
        //    Log.Error("Unhandled Appdomain exception", e.ExceptionObject as Exception);

        //}

        //static void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        //{
        //    Log.Error("Unhandled Appdomain first chance exception", e.Exception);

        //}

        private const string logName = "log";
        private static string _currentLogName;
        private static string _currentLogPath;
        public const long MaxFileSize = 655350;
        private static string GetTimeStamp
        {
            get { return DateTime.Now.ToString("G")+": "; }
        }
        
        private const int LogRotationsCount = 4;
        static Logger()
        {
            InitNewFile();
            using (var writer = new StreamWriter(_currentLogPath,true))
            {
                writer.WriteLine();
                writer.WriteLine("============================");
                writer.WriteLine(GetTimeStamp + "DailyNote started");
            }
        }

        private static void InitNewFile()
        {
            string logDirpath = Environment.CurrentDirectory;
            var dirInfo = new DirectoryInfo(logDirpath);
            var logs = from logfile in dirInfo.GetFiles()
                orderby logfile.CreationTime ascending
                where logfile.Name.StartsWith(logName) && logfile.Extension == ".txt"
                select logfile;
            var loglist = new List<FileInfo>(logs);
            while (loglist.Count() >= LogRotationsCount)
            {
                loglist[0].Delete();
                loglist.RemoveAt(0);
            }
            var dt = DateTime.Now;
            _currentLogName = logName +" "+ dt.ToString("dd-MM-yy_h-mm-ss") + ".txt";
            _currentLogPath = Path.Combine(Environment.CurrentDirectory, _currentLogName);
        }

        private static bool FileSizeExceeded()
        {
            var i = new FileInfo(_currentLogPath);
            return i.Length > MaxFileSize;
        }

        public static void Debug(string message)
        {
            if (LoggerState == LoggerState.Debug)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print("Thread:" + Thread.CurrentThread.Name + "; " + message);
                if (FileSizeExceeded()) InitNewFile();
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "DEBUG: " + message);
                }
            }
        }
        public static void Info(string message)
        {
            if ((int)LoggerState <= (int)LoggerState.Info)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(message);
                if (FileSizeExceeded()) InitNewFile();
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "INFO: " + message);
                }
            }
        }
        public static void Warn(string message)
        {
            if ((int) LoggerState <= (int) LoggerState.Warn)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(message);
                if (FileSizeExceeded()) InitNewFile();
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "WARN: " + message);
                }
            }
        }
        public static void Error(string message)
        {
            if ((int) LoggerState <= (int) LoggerState.Error)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(message);
                if (FileSizeExceeded()) InitNewFile();
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "ERROR: " + message);
                }
            }
        }
        public static void Fatal(string message)
        {
            if ((int) LoggerState <= (int) LoggerState.Error)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(message);
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "FATAL: " + message);
                }
            }
        }

        public static void Error(Exception ex)
        {
            if ((int) LoggerState <= (int) LoggerState.Error)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(ex.Message);
                if (FileSizeExceeded()) InitNewFile();

                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "ERROR: " + ex);
                }
            }
        }
        public static void Fatal(Exception ex)
        {
            if ((int) LoggerState <= (int) LoggerState.Error)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(ex.Message);
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "FATAL: " + ex);
                }
            }
        }
        public static void Error(string message, Exception ex)
        {
            if ((int) LoggerState <= (int) LoggerState.Error)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(message);
                if (LogToOutput) System.Diagnostics.Debug.Print(ex.Message);

                if (FileSizeExceeded()) InitNewFile();
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "ERROR: " + message);
                    writer.WriteLine(GetTimeStamp + ex);
                }
            }
        }
        public static void Fatal(string message, Exception ex)
        {
            if ((int) LoggerState <= (int) LoggerState.Error)
            {
                if (LogToOutput) System.Diagnostics.Debug.Print(message);
                if (LogToOutput) System.Diagnostics.Debug.Print(ex.Message);

                if (FileSizeExceeded()) InitNewFile();
                using (var writer = new StreamWriter(_currentLogPath, true))
                {
                    writer.WriteLine(GetTimeStamp + "FATAL: " + message);
                    writer.WriteLine(GetTimeStamp + ex);
                }
            }
        }
    }
}
