﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DailyNote.ViewModels;

namespace DailyNote
{
    /// <summary>
    /// Interaction logic for CalendarViewer.xaml
    /// </summary>
    public partial class CalendarViewer
    {
        public CalendarViewModel VM { get; set; }
        private bool manualCheckToggle = false;
        public CalendarViewer()
        {
            InitializeComponent();
            DataContext = this;
        }

        public CalendarViewer(CalendarViewModel vm)
        {
            VM = vm;
            InitializeComponent();
            DataContext = this;
            ReformDaysWeeks();
            
            vm.CalendarInvalidated += CalendarInvalidated;
        }

        private void CalendarInvalidated(object sender, EventArgs e)
        {
            ReformDaysWeeks();
        }

        private void ReformDaysWeeks()
        {
            Logger.Debug("CalendarViewer reforming days");
            DaysGrid.Children.Clear();
            foreach (var dayItem in VM.DayVMs)
            {
                int dColumn = dayItem.Key / 7;
                int dRow = (dayItem.Key % 7) + 1;
                var cel = new CElement(dayItem.Value) {Name = "d" + dayItem.Key};
                cel.ApplyTemplate();
                cel.Checked += CElement_Checked; //todo add unsubscription?
                var bndState = new Binding("FilledState") { Source = dayItem.Value };
                cel.SetBinding(CElement.FilledStateProperty, bndState);

                var bndValue = new Binding { Source = dayItem.Value };
                cel.SetBinding(CElement.VMProperty, bndValue);
                
                DaysGrid.Children.Add(cel);
                Grid.SetRow(cel, dRow);
                Grid.SetColumn(cel, dColumn);

            }
            int wColumn = 0;
            const int wRow = 0;
            foreach (var weekItem in VM.WeekVMs)
            {
                var cel = new CElement(weekItem.Value) { Name = "w" + weekItem.Key };
                cel.ApplyTemplate();
                cel.Checked += CElement_Checked; //todo add unsubscription?
                
                var bndState = new Binding("FilledState") { Source = weekItem.Value };
                cel.SetBinding(CElement.FilledStateProperty, bndState);

                var bndValue = new Binding { Source = weekItem.Value };
                cel.SetBinding(CElement.VMProperty, bndValue);

                DaysGrid.Children.Add(cel);
                Grid.SetRow(cel, wRow);
                Grid.SetColumn(cel, wColumn);
                wColumn++;
            }
        }

        private void CElement_Checked(object sender, RoutedEventArgs e)
        {
            var cSender = sender as CElement;
            DeselectToggles(cSender);
            VM.LoadCEVM(cSender.VM);
        }
        private void CElementYear_Checked(object sender, RoutedEventArgs e)
        {
            if (!manualCheckToggle)
            {
                manualCheckToggle = true;
                var cSender = sender as CElement;
                DeselectAllToggles();
                Year2.IsChecked = true;
                VM.LoadCEVM(cSender.VM);
            }
            else manualCheckToggle = false;
        }

        private void DeselectToggles(CElement cSender)
        {
            foreach (var item in DaysGrid.Children)
            {
                if (item is CElement && item != cSender)
                    (item as CElement).IsChecked = false;
            }
            foreach (var item in MonthsGrid.Children)
            {
                if (item is CElement && item != cSender)
                    (item as CElement).IsChecked = false;
            }
            if (Year1 != cSender) Year1.IsChecked = false;
            if (Year2 != cSender) Year2.IsChecked = false;
            if (Year3 != cSender) Year3.IsChecked = false;
        }
        private void DeselectAllToggles()
        {
            foreach (var item in DaysGrid.Children)
            {
                if (item is CElement)
                    (item as CElement).IsChecked = false;
            }
            foreach (var item in MonthsGrid.Children)
            {
                if (item is CElement)
                    (item as CElement).IsChecked = false;
            }
            Year1.IsChecked = false;
            Year2.IsChecked = false;
            Year3.IsChecked = false;
        }

    }
}
