﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DailyNote.ViewModels;

namespace DailyNote
{
    /// <summary>
    /// Interaction logic for SquareControl.xaml
    /// </summary>
    [TemplateVisualState(Name = "Filled", GroupName = "FilledStates")]
    [TemplateVisualState(Name = "Partial", GroupName = "FilledStates")]
    [TemplateVisualState(Name = "Empty", GroupName = "FilledStates")]
    [TemplateVisualState(Name = "Missed", GroupName = "FilledStates")]
    [TemplateVisualState(Name = "Actual", GroupName = "ActualityStates")]
    [TemplateVisualState(Name = "NotActual", GroupName = "ActualityStates")]

    public partial class CElement
    {
        public static bool Res;
        public static readonly DependencyProperty VMProperty =
            DependencyProperty.Register("VM", typeof(CElementViewModel), typeof(CElement), new PropertyMetadata(null));
        public CElementViewModel VM
        {
            get { return (CElementViewModel)GetValue(VMProperty); }
            set { SetValue(VMProperty, value); }
        }
       
        public static DependencyProperty FilledStateProperty = DependencyProperty.Register("FilledState",
        typeof(string), typeof(CElement), new PropertyMetadata("Empty", OnFilledChange));
        public string FilledState
        {
            get { return (string)GetValue(FilledStateProperty); }
            set { SetValue(FilledStateProperty, value); }
        }

        private static void OnFilledChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            bool result=false;
            result = VisualStateManager.GoToState((ContentControl)d, (string)e.NewValue, false);
            Res = result;
        }

        public CElement(CElementViewModel vm)
            :this()
        {
            VM = vm;
        }

        public CElement()
        {
            InitializeComponent();
        }
    }
}
