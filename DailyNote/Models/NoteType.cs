﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace DailyNote.Models
{
    public enum NoteType
    {
        Special = -1, 
        Year = 1,
        Month = 2, 
        Week = 3,
        Day = 4  
    }
}
