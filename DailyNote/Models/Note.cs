﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyNote.Models
{
    public class Note
    {
        public long Id { get; set; }
        public DateTime DateTime { get { return new DateTime((int)Year, (int)Month, (int)Day); } }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        
        public long Year { get; set; }
        public long Month { get; set; }
        public long Week { get; set; }
        public long Day { get; set; }

        private string _motto;
        public string Motto
        {
            get { return _motto; }
            set
            {
                if (_content != value)
                {
                    RecordInvalidated = true;
                }
                _motto = value;
            }
        }
        private string _achievements;
        public string Achievements
        {
            get { return _achievements; }
            set
            {
                if (_content != value)
                {
                    RecordInvalidated = true;
                }
                _achievements = value;
            }
        }
        private string _content;
        public string Content
        {
            get { return _content; }
            set
            {
                if (_content != value)
                {
                    RecordInvalidated = true;
                }
                _content = value;
            }
        }
        public NoteType Type { get; set; }

        public bool IsCurrent
        {
            get
            {
                switch (Type)
                {
                    case NoteType.Year:
                        return DateTime.Now.Year == Year;
                    case NoteType.Month:
                        return DateTime.Now.Year == Year
                            && DateTime.Now.Month == Month;
                    case NoteType.Day:
                        return DateTime.Now.Year == Year
                            && DateTime.Now.Month == Month
                            && DateTime.Now.Day == Day;
                }
                return false;
            }
        }

        public bool RecordInvalidated {get; set;}

        public Note()
        {
            Id = -1;
            Created = DateTime.Now;
            Edited = DateTime.Now;
            Year = 1989;
            Month = 1;
            Day = 1;
            _motto = "";
            _achievements = "";
            _content = "";
        }

        public Dictionary<string, string> GetDBFormat()
        {
            var dic = new Dictionary<string, string>
            {
                {"created", Created.ToString("yyyy-MM-dd")},
                {"edited", Edited.ToString("yyyy-MM-dd")},
                {"year", Year.ToString(CultureInfo.InvariantCulture)},
                {"month",Month.ToString(CultureInfo.InvariantCulture)},
                {"week", Week.ToString(CultureInfo.InvariantCulture)},
                {"day", Day.ToString(CultureInfo.InvariantCulture)},
                {"type", ((int)Type).ToString(CultureInfo.InvariantCulture)},
                {"motto", Motto},
                {"achievements", Achievements},
                {"content", Content}
            };
            return dic;
        }
    }
}
