﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyNote.Models;

namespace DailyNote
{
    public class NoteDBHelper:DBHelper
    {
        private List<Note> _monthCache;
        private List<Note> _weeksCache;
        private List<Note> _daysCache;
        private Note _yearNoteCache;
        private int _cachedYear;

        public NoteDBHelper()
        {
            
        }
        
        public void LoadCache(int year)
        {
            Logger.Debug("DB Caching notes for year " + year);
            _cachedYear = year;
            var ie = GetNotes("type=1 AND year=" + year);
            if (ie.Count > 0) _yearNoteCache = ie[0];
            _monthCache = GetNotes("type=2 AND year=" + year);
            _weeksCache = GetNotes("type=3 AND year=" + year);
            _daysCache = GetNotes("type=4 AND year=" + year);
        }

        public bool SaveNote(Note note)
        {
            Logger.Debug("DB Saving note of date " + note.DateTime);
            if (note.Year == _cachedYear)
            {
                switch (note.Type)
                {
                    case NoteType.Day:
                        var indd = _daysCache.FindIndex(n => n.Day == note.Day && n.Month == note.Month);
                        if (indd != -1)
                            _daysCache.RemoveAt(indd);
                        _daysCache.Add(note);
                        break;
                    case NoteType.Week:
                        var indw = _weeksCache.FindIndex(n => n.Week == note.Week && n.Month == note.Month);
                        if (indw != -1)
                            _weeksCache.RemoveAt(indw);
                        _weeksCache.Add(note);
                        break;
                    case NoteType.Month:
                        var indy = _monthCache.FindIndex(n => n.Month == note.Month);
                        if (indy != -1)
                            _monthCache.RemoveAt(indy);
                        _monthCache.Add(note);
                        break;
                    case NoteType.Year:
                        _yearNoteCache = note;
                        break;
                }
            }

            string whereString = "year=" + note.Year + " AND "
                                + "month=" + note.Month + " AND "
                                + "week=" + note.Week + " AND "
                                + "day=" + note.Day + " AND "
                                + "type=" + (int)note.Type;
            bool result;
            var dic = GetNotes(whereString);
            if (dic.Count > 0)
            {
                Logger.Debug("Saving via update");
                result = Update(PrimaryTableName, note.GetDBFormat(), whereString);
            }
            else
            {
                int num;
                result = Insert(PrimaryTableName, note.GetDBFormat(), out num);
                note.Id = num;
                Logger.Debug("** Saving as new, id=" + num);
            }
            note.RecordInvalidated = false;
            return result;
        }
        public List<Note> GetNotes(string where = "")
        {
            Logger.Debug("DB GetNotes " + where);
            var result = new List<Note>();
            var cnn = new SQLiteConnection(DBConnection);
            cnn.Open();
            var mycommand = new SQLiteCommand(cnn)
            {
                CommandText =
                    String.IsNullOrEmpty(@where)
                        ? String.Format("select * from {0};", PrimaryTableName)
                        : String.Format("select * from {0} where {1};", PrimaryTableName, @where)
            };
            var reader = mycommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var note = new Note
                    {
                        Created = (DateTime)reader["created"],
                        Edited = (DateTime)reader["edited"],
                        Year = (long)(reader["year"]),
                        Month = (long)reader["month"],
                        Week = (long)reader["week"],
                        Day = (long)reader["day"],
                        Motto = (string)reader["motto"],
                        Achievements = (string)reader["achievements"],
                        Content = (string)reader["content"]
                    };
                    switch ((long)reader["type"])
                    {
                        case 0:
                            note.Type = NoteType.Special;
                            break;
                        case 1:
                            note.Type = NoteType.Year;
                            break;
                        case 2:
                            note.Type = NoteType.Month;
                            break;
                        case 3:
                            note.Type = NoteType.Week;
                            break;
                        case 4:
                            note.Type = NoteType.Day;
                            break;
                    }
                    note.RecordInvalidated = false;
                    result.Add(note);
                }
            }
            cnn.Close();
            Logger.Debug("DB Got " + result.Count + " notes");
            return result;
        }
        public Note GetMonthNote(int year, int month)
        {
            Logger.Debug("DB GetMonthNote note of date " + month + "." + year);
            if (year != _cachedYear)
            {
                Logger.Warn("DB Cache miss! Requested month " + month + " note for " + year + ", cached year " + _cachedYear);
                string whereString = "year=" + year + " AND "
                                + "month=" + month + " AND "
                                + "type=" + (int)NoteType.Month;
                var ie = GetNotes(whereString);
                if (ie.Count > 0) return ie.ElementAt(0);
                return new Note { Year = year, Month = month, Type = NoteType.Month };
            }
            var note = _monthCache.Find(n => n.Month == month);
            if (note != null) return note;
            note = new Note { Year = _cachedYear, Month = month, Type = NoteType.Month };
            _monthCache.Add(note);
            return note;
        }
        public Note GetWeekNote(int year, int month, int week)
        {
            Logger.Debug("DB GetWeekNote note of date " + week + "w " + month + "." + year);
            if (year != _cachedYear)
            {
                Logger.Warn("DB Cache miss! Requested month " + month + ", week " + week + " note for " + year +
                            ", cached year " + _cachedYear);
                string whereString = "year=" + year + " AND "
                                + "month=" + month + " AND "
                                + "week=" + week + " AND "
                                + "type=" + (int)NoteType.Week;
                var ie = GetNotes(whereString);
                if (ie.Count > 0) return ie.ElementAt(0);
                return new Note { Year = year, Month = month, Week = week, Type = NoteType.Week };
            }
            var note = _weeksCache.Find(n => n.Month == month && n.Week == week);
            if (note != null) return note;
            note = new Note { Year = _cachedYear, Month = month, Week = week, Type = NoteType.Week };
            _weeksCache.Add(note);
            return note;
        }
        public List<Note> GetWeekNotes(int month)
        {
            var notes = _weeksCache.FindAll(n => n.Month == month);
            return notes;
        }
        public Note GetDayNote(int year, int month, int day)
        {
            Logger.Debug("DB GetDayNote note of date " + day + "." + month + "." + year);
            if (year != _cachedYear)
            {
                Logger.Warn("DB Cache miss! Requested month " + month + ", day " + day + " note for " + year +
                            ", cached year " + _cachedYear);
                string whereString = "year=" + year + " AND "
                                + "month=" + month + " AND "
                                + "day=" + day + " AND "
                                + "type=" + (int)NoteType.Day;
                var ie = GetNotes(whereString);
                if (ie.Count > 0) return ie.ElementAt(0);
                return new Note { Year = year, Month = month, Day = day, Type = NoteType.Day };
            }
            var note = _daysCache.Find(n => n.Month == month && n.Day == day);
            if (note != null) return note;
            note = new Note { Year = _cachedYear, Month = month, Day = day, Type = NoteType.Day };
            _daysCache.Add(note);
            return note;
        }
        public List<Note> GetDayNotes(int month)
        {
            var notes = _daysCache.FindAll(n => n.Month == month);
            return notes;
        }
        public Note GetYearNote(int year)
        {
            if (year != _cachedYear || _yearNoteCache == null)
            {
                Logger.Warn("DB Cache miss! Requested year " + year + " note, cache was " + (_yearNoteCache == null ? "null" : _yearNoteCache.ToString()));
                string whereString = "year=" + year + " AND "
                                + "type=" + (int)NoteType.Year;
                var ie = GetNotes(whereString);
                if (ie.Count > 0) return ie.ElementAt(0);
                _yearNoteCache = new Note { Year = year, Type = NoteType.Year };
            }
            return _yearNoteCache;
        }

        public void AddTestNote()
        {
            ClearTable(PrimaryTableName);
            var test = new Note
            {
                Edited = DateTime.Now,
                Year = 2014,
                Month = 12,
                Week = 4,
                Day = 18,
                Type = NoteType.Day,
                Motto = "Motto",
                Achievements = "Achievements",
                Content = "Content"
            };
            int num;
            Insert("notes", test.GetDBFormat(), out num);
        }

        protected override void ConstructTable()
        {
            ExecuteNonQuery(
                "CREATE TABLE " + PrimaryTableName +
                "(id INTEGER, " +
                "created DATE, " +
                "edited DATE, " +
                "year INTEGER NOT NULL, " +
                "month INTEGER NOT NULL, " +
                "week INTEGER NOT NULL, " +
                "day INTEGER NOT NULL, " +
                "type INTEGER NOT NULL, " +
                "motto VARCHAR, " +
                "achievements VARCHAR, " +
                "content VARCHAR, " +
                "PRIMARY KEY (year, month,week,day,type));");
        }
    }
}
