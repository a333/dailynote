﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DailyNote.Models;

namespace DailyNote.ViewModels
{
    public delegate void NoteChangedEventHandler(object sender, NoteEventArgs e);

    public class CalendarViewModel : ViewModelBase
    {
        private bool _actual = true;
        public bool Actual
        {
            get { return _actual; }
            set
            {
                _actual = value;
                OnPropertyChanged("Actual");
            }
        }

        private bool _current;
        public bool Current
        {
            get { return _current; }
            set
            {
                _current = value;
                OnPropertyChanged("Current");
            }
        }

        public event NoteChangedEventHandler NoteSelectionChanged;
        public event EventHandler CalendarInvalidated;
        private NoteDBHelper _db;

        public Dictionary<int, CElementViewModel> MonthVMs { get; set; }
        public Dictionary<int, CElementViewModel> WeekVMs { get; set; }
        public Dictionary<int, CElementViewModel> DayVMs { get; set; }

        private int _selectedYear;
        public int SelectedYear
        {
            get { return _selectedYear; }
        }
        private int _selectedMonth;
        public int SelectedMonth
        {
            get { return _selectedMonth; }
        }
        private int _selectedDay;
        public int SelectedDay
        {
            get { return _selectedDay; }
        }

        private CElementViewModel _year1;
        public CElementViewModel Year1
        {
            get { return _year1; }
            set
            {
                _year1 = value;
                OnPropertyChanged("Year1");
            }
        }
        private CElementViewModel _year2;
        public CElementViewModel Year2
        {
            get { return _year2; }
            set
            {
                _year2 = value;
                OnPropertyChanged("Year2");
            }
        }
        
        private CElementViewModel _year3;
        public CElementViewModel Year3
        {
            get { return _year3; }
            set
            {
                _year3 = value;
                OnPropertyChanged("Year3");
            }
        }

        #region Months

        private CElementViewModel _monthJan;
        public CElementViewModel MonthJan
        {
            get { return _monthJan; }
            set
            {
                _monthJan = value;
                OnPropertyChanged("MonthJan");
            }
        }

        private CElementViewModel _monthFeb;
        public CElementViewModel MonthFeb
        {
            get { return _monthFeb; }
            set
            {
                _monthFeb = value;
                OnPropertyChanged("MonthFeb");
            }
        }

        private CElementViewModel _monthMar;
        public CElementViewModel MonthMar
        {
            get { return _monthMar; }
            set
            {
                _monthMar = value;
                OnPropertyChanged("MonthMar");
            }
        }

        private CElementViewModel _monthApr;
        public CElementViewModel MonthApr
        {
            get { return _monthApr; }
            set
            {
                _monthApr = value;
                OnPropertyChanged("MonthApr");
            }
        }

        private CElementViewModel _monthMay;
        public CElementViewModel MonthMay
        {
            get { return _monthMay; }
            set
            {
                _monthMay = value;
                OnPropertyChanged("MonthMay");
            }
        }

        private CElementViewModel _monthJun;
        public CElementViewModel MonthJun
        {
            get { return _monthJun; }
            set
            {
                _monthJun = value;
                OnPropertyChanged("MonthJun");
            }
        }

        private CElementViewModel _monthJul;
        public CElementViewModel MonthJul
        {
            get { return _monthJul; }
            set
            {
                _monthJul = value;
                OnPropertyChanged("MonthJul");
            }
        }

        private CElementViewModel _monthAug;
        public CElementViewModel MonthAug
        {
            get { return _monthAug; }
            set
            {
                _monthAug = value;
                OnPropertyChanged("MonthAug");
            }
        }

        private CElementViewModel _monthSep;
        public CElementViewModel MonthSep
        {
            get { return _monthSep; }
            set
            {
                _monthSep = value;
                OnPropertyChanged("MonthSep");
            }
        }

        private CElementViewModel _monthOct;
        public CElementViewModel MonthOct
        {
            get { return _monthOct; }
            set
            {
                _monthOct = value;
                OnPropertyChanged("MonthOct");
            }
        }

        private CElementViewModel _monthNov;
        public CElementViewModel MonthNov
        {
            get { return _monthNov; }
            set
            {
                _monthNov = value;
                OnPropertyChanged("MonthNov");
            }
        }

        private CElementViewModel _monthDec;
        public CElementViewModel MonthDec
        {
            get { return _monthDec; }
            set
            {
                _monthDec = value;
                OnPropertyChanged("MonthDec");
            }
        }

        #endregion

        //DateTime _firstRecordDate;
        public CalendarViewModel(NoteDBHelper db)
        {
            DayVMs = new Dictionary<int, CElementViewModel>();
            WeekVMs = new Dictionary<int, CElementViewModel>();
            MonthVMs = new Dictionary<int, CElementViewModel>();
            _db = db;
            //_firstRecordDate = _db.GetFirstRecordDate();
        }

        private void LoadDate(int year, int month = 1, int day = 1, bool isWeek=false)
        {
            Logger.Debug("CVM LoadDate " + day + "." + month+"."+year);
            if (year != _selectedYear)
            {
                _db.LoadCache(year);
                FillYears(year);
                FillMonths(month);
                _selectedDay = day;
                FillDaysAndWeeks();
                OnCalendarInvalidated();
            }
            else if (month != _selectedMonth)
            {
                FillMonths(month);
                _selectedDay = day;
                FillDaysAndWeeks();
                OnCalendarInvalidated();
            }
            else 
            {
                _selectedDay = day;
                foreach (var item in DayVMs.Values)
                {
                    if (!isWeek 
                        && item.Note.DateTime.Day == _selectedDay 
                        && item.Note.DateTime.Month == _selectedMonth)
                        item.Selected = true;
                    else item.Selected = false;
                }
                
            }
        }

        public void LoadCEVM(CElementViewModel vm)
        {
            Logger.Debug("CVM Loading cevm for "+vm.Note.DateTime.ToString("d"));
            OnNoteSelectionChanged(vm);
            var note = vm.Note;
            //if(note.Type!=NoteType.Week)
            LoadDate((int)note.Year,(int)note.Month,(int)note.Day,note.Type==NoteType.Week);
            //else
                //LoadDate((int)note.Year,(int)note.Month);
            
        }

        private void FillYears(int year)
        {
            Logger.Debug("CVM FillYears " + year);
            _selectedYear = year;
            var years = _db.GetNotes("type=1");
            var note1 = years.Find((n) => n.Year == SelectedYear - 1) ??
                        new Note { Year = SelectedYear - 1, Type = NoteType.Year };
            var note2 = years.Find((n) => n.Year == SelectedYear) ??
                        new Note { Year = SelectedYear, Type = NoteType.Year };
            var note3 = years.Find((n) => n.Year == SelectedYear + 1) ??
                        new Note { Year = SelectedYear + 1, Type = NoteType.Year };
            Year1 = new CElementViewModel(note1);
            Year2 = new CElementViewModel(note2) {Selected = true};
            Year3 = new CElementViewModel(note3);
        }

        private void FillMonths(int selectedMonth)
        {
            Logger.Debug("CVM FillMonths " + selectedMonth);
            MonthJan = new CElementViewModel(_db.GetMonthNote(_selectedYear, 1));
            MonthFeb = new CElementViewModel(_db.GetMonthNote(_selectedYear, 2));
            MonthMar = new CElementViewModel(_db.GetMonthNote(_selectedYear, 3));
            MonthApr = new CElementViewModel(_db.GetMonthNote(_selectedYear, 4));
            MonthMay = new CElementViewModel(_db.GetMonthNote(_selectedYear, 5));
            MonthJun = new CElementViewModel(_db.GetMonthNote(_selectedYear, 6));
            MonthJul = new CElementViewModel(_db.GetMonthNote(_selectedYear, 7));
            MonthAug = new CElementViewModel(_db.GetMonthNote(_selectedYear, 8));
            MonthSep = new CElementViewModel(_db.GetMonthNote(_selectedYear, 9));
            MonthOct = new CElementViewModel(_db.GetMonthNote(_selectedYear, 10));
            MonthNov = new CElementViewModel(_db.GetMonthNote(_selectedYear, 11));
            MonthDec = new CElementViewModel(_db.GetMonthNote(_selectedYear, 12));

            MonthVMs.Clear();
            MonthVMs.Add(1, MonthJan);
            MonthVMs.Add(2, MonthFeb);
            MonthVMs.Add(3, MonthMar);
            MonthVMs.Add(4, MonthApr);
            MonthVMs.Add(5, MonthMay);
            MonthVMs.Add(6, MonthJun);
            MonthVMs.Add(7, MonthJul);
            MonthVMs.Add(8, MonthAug);
            MonthVMs.Add(9, MonthSep);
            MonthVMs.Add(10, MonthOct);
            MonthVMs.Add(11, MonthNov);
            MonthVMs.Add(12, MonthDec);
            _selectedMonth = selectedMonth;

            MonthVMs[_selectedMonth].Selected = true;
        }

        private void FillDaysAndWeeks()
        {
            Logger.Debug("CVM FillDaysAndWeeks");
            WeekVMs.Clear();
            DayVMs.Clear();
            var dt = new DateTime(_selectedYear, _selectedMonth, 1);
            var weekOffset = 0;
            if (dt.DayOfWeek == DayOfWeek.Monday)
                weekOffset = 7;
            var dayOffset = (int) dt.DayOfWeek;
            if (dayOffset == 0) dayOffset = 6;
            else dayOffset--;

            for (int i = 0; i < 42; i++)
            {
                int dayNum = i - weekOffset - dayOffset;
                var dtNew = dt.AddDays(dayNum);
                var note = _db.GetDayNote(dtNew.Year,dtNew.Month, dtNew.Day);
                var vm = new CElementViewModel(note);
                if (dtNew.Month != dt.Month || dtNew.Year != dt.Year)
                    vm.Actual = false;
                if (dtNew.Month == dt.Month && dtNew.Day == dt.Day)
                    vm.Selected = true;
                DayVMs.Add(i, vm);
                
                if (i%7 != 0) continue;

                Note weekNote = _db.GetWeekNote(dtNew.Year, dtNew.Month, GetIso8601WeekOfYear(dtNew));
                var wvm = new CElementViewModel(weekNote);
                if (dtNew.Month != dt.Month || dtNew.Year != dt.Year)
                    wvm.Actual = false;
                WeekVMs.Add(i, wvm);
            }
            
        }
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        } 
       
        private ICommand _nextYearCommand;
        public ICommand NextYearCommand
        {
            get
            {
                return _nextYearCommand ??
                       (_nextYearCommand = new RelayCommand(NextYear));
            }
        }
        private void NextYear()
        {
            LoadDate(_selectedYear + 1);
        }

        private ICommand _prevYearCommand;
        public ICommand PrevYearCommand
        {
            get
            {
                return _prevYearCommand ??
                       (_prevYearCommand = new RelayCommand(PrevYear));
            }
        }
        private void PrevYear()
        {
            LoadDate(_selectedYear - 1);
        }

        public void OnNoteSelectionChanged(CElementViewModel vm)
        {
            if (NoteSelectionChanged != null)
                NoteSelectionChanged(this, new NoteEventArgs { VM = vm });
        }
        public void OnCalendarInvalidated()
        {
            if (CalendarInvalidated != null)
                CalendarInvalidated(this, new EventArgs());
        }

        internal void Init(DateTime dateTime)
        {
            Logger.Debug("CVM Init to " + dateTime.ToShortDateString());
            LoadDate(dateTime.Year, dateTime.Month, dateTime.Day);
            var vm =
                DayVMs.Values.ToList()
                    .Find(
                        (vmi =>
                            vmi.Note.Year == dateTime.Year 
                            && vmi.Note.Month == dateTime.Month 
                            && vmi.Note.Day == dateTime.Day));
            if(vm!=null)
                LoadCEVM(vm);
        }
    }
}
