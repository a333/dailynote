﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DailyNote.Models;

namespace DailyNote.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private NoteDBHelper _db;
        private CalendarViewModel _cvm;
        private CElementViewModel _currentCEVM;
        private System.Windows.Threading.DispatcherTimer _saveTimer;

        private string _noteContent;
        public string NoteContent
        {
            get { return _noteContent; }
            set
            {
                if (_saveTimer.IsEnabled)
                    _saveTimer.Stop();
                _saveTimer.Start();
                _noteContent = value;
                OnPropertyChanged("NoteContent");
            }
        }

        //todo store date components discreetly + store type of the item.

        private string _mottoEdit;
        public string MottoEdit
        {
            get { return _mottoEdit; }
            set
            {
                if (_saveTimer.IsEnabled)
                    _saveTimer.Stop();
                _saveTimer.Start();
                _mottoEdit = value;
                OnPropertyChanged("MottoEdit");
            }
        }
        private string _motto1;
        public string Motto1
        {
            get { return _motto1; }
            set
            {
                _motto1 = value;
                OnPropertyChanged("Motto1");
            }
        }
        private string _motto2;
        public string Motto2
        {
            get { return _motto2; }
            set
            {
                _motto2 = value;
                OnPropertyChanged("Motto2");
            }
        }
        private string _motto3;
        public string Motto3
        {
            get { return _motto3; }
            set
            {
                _motto3 = value;
                OnPropertyChanged("Motto3");
            }
        }
        private Visibility _mottoEditVis = Visibility.Collapsed;
        public Visibility MottoEditVis
        {
            get { return _mottoEditVis; }
            set
            {
                _mottoEditVis = value;
                OnPropertyChanged("MottoEditVis");
            }
        }
        private Visibility _motto1Vis = Visibility.Collapsed;
        public Visibility Motto1Vis
        {
            get { return _motto1Vis; }
            set
            {
                _motto1Vis = value;
                OnPropertyChanged("Motto1Vis");
            }
        }
        private Visibility _motto2Vis = Visibility.Collapsed;
        public Visibility Motto2Vis
        {
            get { return _motto2Vis; }
            set
            {
                _motto2Vis = value;
                OnPropertyChanged("Motto2Vis");
            }
        }
        private Visibility _motto3Vis = Visibility.Collapsed;
        public Visibility Motto3Vis
        {
            get { return _motto3Vis; }
            set
            {
                _motto3Vis = value;
                OnPropertyChanged("Motto3Vis");
            }
        }

        private string _achievements;
        public string Achievements
        {
            get { return _achievements; }
            set
            {
                if(_saveTimer.IsEnabled)
                    _saveTimer.Stop();
                _saveTimer.Start();
                _achievements = value;
                OnPropertyChanged("Achievements");
            }
        }

        public MainViewModel(NoteDBHelper db, CalendarViewModel cvm)
        {
            _cvm = cvm;
            _db = db;
            _saveTimer = new System.Windows.Threading.DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0,600)};
            _saveTimer.Tick += _saveTimer_Tick;
        }

        void _saveTimer_Tick(object sender, EventArgs e)
        {
            SaveNote();
            _saveTimer.Stop();
        }

        public void InsertTimeStamp()
        {

        }

        public void SaveNote()
        {
            Logger.Debug("MainVM Saving note");
            if (_currentCEVM != null)
            {
                _currentCEVM.Note.Content = NoteContent;
                _currentCEVM.Note.Achievements = Achievements;
                if(MottoEditVis==Visibility.Visible) 
                    _currentCEVM.Note.Motto = MottoEdit;
                if (_currentCEVM.Note.RecordInvalidated)
                {
                    _db.SaveNote(_currentCEVM.Note);
                    
                }
                _currentCEVM.InvalidateData();
            }
        }

        public void SetCEVM(CElementViewModel vm)
        {
            Logger.Debug("MainVM set vm to " +vm.Note.DateTime+" "+vm.Note.Type.ToString());
            SaveNote();
            _currentCEVM = vm;
            
            Note note = vm.Note;
            int year = (int)note.Year;
            int month = (int)note.Month;
            int week = (int)note.Week;
            int day = (int)note.Day;
            switch (note.Type)
            {
                case NoteType.Year:
                    MottoEdit = _currentCEVM.Note.Motto;
                    MottoEditVis = Visibility.Visible;
                    Motto1Vis = Visibility.Collapsed;
                    Motto2Vis = Visibility.Collapsed;
                    Motto3Vis = Visibility.Collapsed;
                    break;
                case NoteType.Month:
                    MottoEdit = _currentCEVM.Note.Motto;
                    MottoEditVis = Visibility.Visible;
                    Motto1Vis = Visibility.Collapsed;
                    Motto2Vis = Visibility.Collapsed;
                    
                    Motto3 = _db.GetYearNote(year).Motto;
                    Motto3Vis = String.IsNullOrEmpty(Motto3) ? Visibility.Collapsed : Visibility.Visible;

                    break;
                case NoteType.Week:
                    MottoEdit = _currentCEVM.Note.Motto;
                    MottoEditVis = Visibility.Visible;
                    Motto1Vis = Visibility.Collapsed;
                    
                    Motto2 = _db.GetYearNote(year).Motto;
                    Motto2Vis = String.IsNullOrEmpty(Motto2) ? Visibility.Collapsed : Visibility.Visible;
                    
                    Motto3 = _db.GetMonthNote(year,month).Motto;
                    Motto3Vis = String.IsNullOrEmpty(Motto3) ? Visibility.Collapsed: Visibility.Visible;
                    
                    break;
                case NoteType.Day:
                    MottoEdit = "";
                    MottoEditVis = Visibility.Collapsed;
                    
                    Motto1 = _db.GetYearNote(year).Motto;
                    Motto1Vis = String.IsNullOrEmpty(Motto1) ? Visibility.Collapsed : Visibility.Visible;

                    Motto2 = _db.GetMonthNote(year, month).Motto;
                    Motto2Vis = String.IsNullOrEmpty(Motto2) ? Visibility.Collapsed : Visibility.Visible;
                    
                    Motto3 = _db.GetWeekNote(year,month,CalendarViewModel.GetIso8601WeekOfYear(new DateTime(year,month,day))).Motto;
                    Motto3Vis = String.IsNullOrEmpty(Motto3) ? Visibility.Collapsed: Visibility.Visible;
                    break;
            }
            NoteContent = _currentCEVM.Note.Content;
            Achievements = _currentCEVM.Note.Achievements;
            
        }

        internal void ClearDB()
        {
            _db.ClearDB();
            _db.RecreateBase();
        }
    }
}
