﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DailyNote.Models;

namespace DailyNote.ViewModels
{
    public class NoteEventArgs:EventArgs
    {
        public CElementViewModel VM { get; set; }
    }
}
