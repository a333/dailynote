﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DailyNote.Models;

namespace DailyNote.ViewModels
{
    public class CElementViewModel : ViewModelBase
    {
        private bool _actual=true;
        public bool Actual
        {
            get { return _actual; }
            set
            {
                _actual = value;
                OnPropertyChanged("Actual");
            }
        }

        private double _currentMarkOpacity;
        public double CurrentMarkOpacity
        {
            get { return _currentMarkOpacity; }
            set
            {
                _currentMarkOpacity = value;
                OnPropertyChanged("Current");
            }
        }

        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnPropertyChanged("Selected");
            }
        }

        public DateTime DateTime
        {
            get { return Note.DateTime; }
        }
        
        private Note _note;
        public Note Note
        {
            get { return _note; }
            set
            {
                _note = value;
                OnPropertyChanged("Note");
            }
        }

        private string _filledState;
        public string FilledState
        {
            get { return _filledState; }
            set
            {
                _filledState = value;
                OnPropertyChanged("FilledState");
            }
        }

        public string DisplayValue
        {
            get
            {
                switch (Note.Type)
                {
                    case NoteType.Year:
                        return Note.Year.ToString(CultureInfo.InvariantCulture);
                    case NoteType.Month:
                        return Note.DateTime.ToString("MMM");
                    case NoteType.Week:
                        return Note.Week.ToString(CultureInfo.InvariantCulture);
                    case NoteType.Day:
                        return Note.Day.ToString(CultureInfo.InvariantCulture);
                }
                return "Spec";
            }
        }

        public CElementViewModel(Note note)
        {
            Note = note;
            //_firstRecordDate = firstRecord;
            UpdateNote();
        }

        private void UpdateNote()
        {
            if (_note != null)
            {
                if (!String.IsNullOrEmpty(_note.Content)
                    && !String.IsNullOrEmpty(_note.Achievements)
                   )
                    _filledState = "Filled";
                else if (!String.IsNullOrEmpty(_note.Content)
                         || !String.IsNullOrEmpty(_note.Achievements)
                         )
                    _filledState = "Partial";
                else
                {
                    _filledState = "Empty";
                    DateTime noteMissDate=DateTime.Now;
                    switch (Note.Type)
                    {
                        case NoteType.Year:
                            noteMissDate = new DateTime((int)_note.Year, 12, 31);
                            break;
                        case NoteType.Month:
                            noteMissDate = new DateTime((int)_note.Year, (int)_note.Month, DateTime.DaysInMonth((int)_note.Year, (int)_note.Month));
                            break;
                        case NoteType.Week:
                            int wday = ((int)Note.Week - 1)*7;
                            var dto = new DateTime((int)_note.Year, (int)_note.Month, (int)_note.Day);
                            dto=dto.AddDays(wday);
                            int weekOffset = (int)dto.DayOfWeek;
                            if (weekOffset == 0) weekOffset = 6;
                            else weekOffset--;
                            weekOffset = 6-weekOffset;
                            noteMissDate = dto.AddDays(weekOffset);
                            break;
                        case NoteType.Day:
                            noteMissDate = new DateTime((int)_note.Year, (int)_note.Month, (int)_note.Day);
                            noteMissDate = noteMissDate.AddDays(1);
                            break;
                    }
                    if (noteMissDate < DateTime.Now)
                        _filledState = "Missed";
                }
                OnPropertyChanged("FilledState");
                _currentMarkOpacity = _note.IsCurrent ? 1 : 0;
            }
            else throw new NullReferenceException();
        }

        public CElementViewModel()
        {
            Note = new Note();
        }

        public void InvalidateData()
        {
            UpdateNote();
        }

        private ICommand _fillThisCommand;

        public ICommand FillThisCommand
        {
            get
            {
                return _fillThisCommand ??
                       (_fillThisCommand = new RelayCommand(FillThis));
            }
        }

        private void FillThis()
        {
            
        }

        internal void UpdateMis(object sender, System.Windows.RoutedEventArgs e)
        {
            FilledState = FilledState == "Missed" ? "Filled" : "Missed";
        }

        
    }
}
