﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using DailyNote.Models;

namespace DailyNote
{
    public class DBHelper
    {
        public const string DataBaseName = "database.sqlite";
        public const string PrimaryTableName = "notes";
        protected String DBConnection;
       
        /// <summary>
        ///     Default Constructor for SQLiteDatabase Class.
        /// </summary>
        public DBHelper()
        {
            if (!File.Exists(Path.Combine(Environment.CurrentDirectory, DataBaseName)))
            {
                RecreateBase();
                
            } else DBConnection = "Data Source="+DataBaseName;
            Logger.Info("DBHelper Opened database " + DataBaseName);
        }

        /// <summary>
        ///     Single Param Constructor for specifying the DB file.
        /// </summary>
        /// <param name="inputFile">The File containing the DB</param>
        public DBHelper(String inputFile)
        {
            DBConnection = String.Format("Data Source={0}", inputFile);
            Logger.Info("DB Opened database " + inputFile);
        }

        /// <summary>
        ///     Single Param Constructor for specifying advanced connection options.
        /// </summary>
        /// <param name="connectionOpts">A dictionary containing all desired options and their values</param>
        public DBHelper(Dictionary<String, String> connectionOpts)
        {
            String str = connectionOpts.Aggregate("", (current, row) => current + String.Format("{0}={1}; ", row.Key, row.Value));
            str = str.Trim().Substring(0, str.Length - 1);
            DBConnection = str;
        }

        

        /// <summary>
        ///     Allows the programmer to run a query against the Database.
        /// </summary>
        /// <param name="sql">The SQL to run</param>
        /// <returns>A DataTable containing the result set.</returns>
        public DataTable GetDataTable(string sql)
        {
            var dt = new DataTable();
            try
            {
                var cnn = new SQLiteConnection(DBConnection);
                cnn.Open();
                var mycommand = new SQLiteCommand(cnn) {CommandText = sql};
                var reader = mycommand.ExecuteReader();
                dt.Load(reader);
                reader.Close();
                cnn.Close();
            }
            catch (Exception fail)
            {
                Logger.Error(fail);
            }
            return dt;
        }

        /// <summary>
        ///     Allows the programmer to interact with the database for purposes other than a query.
        /// </summary>
        /// <param name="sql">The SQL to be run.</param>
        /// <returns>An Integer containing the number of rows updated.</returns>
        public int ExecuteNonQuery(string sql)
        {
            var cnn = new SQLiteConnection(DBConnection);
            cnn.Open();
            var mycommand = new SQLiteCommand(cnn) {CommandText = sql};
            var rowsUpdated = mycommand.ExecuteNonQuery();
            cnn.Close();
            return rowsUpdated;
        }

        /// <summary>
        ///     Allows the programmer to retrieve single items from the DB.
        /// </summary>
        /// <param name="sql">The query to run.</param>
        /// <returns>A string.</returns>
        public string ExecuteScalar(string sql)
        {
            var cnn = new SQLiteConnection(DBConnection);
            cnn.Open();
            var mycommand = new SQLiteCommand(cnn) {CommandText = sql};
            var value = mycommand.ExecuteScalar();
            cnn.Close();
            if (value != null)
            {
                return value.ToString();
            }
            return "";
        }


        /// <summary>
        ///     Allows the programmer to easily update rows in the DB.
        /// </summary>
        /// <param name="tableName">The table to update.</param>
        /// <param name="data">A dictionary containing Column names and their new values.</param>
        /// <param name="where">The where clause for the update statement.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Update(String tableName, Dictionary<String, String> data, String where)
        {
            String vals = "";
            Boolean returnCode = true;
            if (data.Count >= 1)
            {
                vals = data.Aggregate(vals, (current, val) => current + String.Format(" {0} = '{1}',", val.Key, val.Value.Replace("'", "''")));
                vals = vals.Substring(0, vals.Length - 1);
            }
            try
            {
                ExecuteNonQuery(String.Format("update {0} set {1} where {2};", tableName, vals, where));
            }
            catch (Exception fail)
            {
                Logger.Error(fail);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily delete rows from the DB.
        /// </summary>
        /// <param name="tableName">The table from which to delete.</param>
        /// <param name="where">The where clause for the delete.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Delete(String tableName, String where)
        {
            Logger.Debug("DB Deleting record " + where);
            var returnCode = true;
            try
            {
                ExecuteNonQuery(String.Format("delete from {0} where {1};", tableName, where));
                
            }
            catch (Exception fail)
            {
                Logger.Error(fail);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily insert into the DB
        /// </summary>
        /// <param name="tableName">The table into which we insert the data.</param>
        /// <param name="data">A dictionary containing the column names and data for the insert.</param>
        /// <param name="num"></param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Insert(String tableName, Dictionary<String, String> data, out int num)
        {
            String columns = "";
            String values = "";
            num = -1;
            Boolean returnCode = true;
            foreach (KeyValuePair<String, String> val in data)
            {
                columns += String.Format(" {0},", val.Key);
                values += String.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);
            try
            {
                Logger.Debug("DB Inserting record " + values);
                num = ExecuteNonQuery(String.Format("insert into {0}({1}) values({2});", tableName, columns, values));
            }
            catch (Exception fail)
            {
                Logger.Error(fail);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily delete all data from the DB.
        /// </summary>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ClearDB()
        {
            Logger.Warn("DB Clearing Database!");
            try
            {
                DataTable tables = GetDataTable("select NAME from SQLITE_MASTER where type='table' order by NAME;");
                foreach (DataRow table in tables.Rows)
                {
                    ClearTable(table["NAME"].ToString());
                }
                return true;
            }
            catch (Exception fail)
            {
                Logger.Error(fail);
                return false;
            }
        }

        /// <summary>
        ///     Allows the user to easily clear all data from a specific table.
        /// </summary>
        /// <param name="table">The name of the table to clear.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ClearTable(String table)
        {
            Logger.Warn("DB Clearing table " + table + "!");
            try
            {
                ExecuteNonQuery(String.Format("delete from {0};", table));
                
                return true;
            }
            catch
            {
                return false;
            }
        }



        public void RecreateBase()
        {
            Logger.Info("DB Recreating base...");
            if (File.Exists(DataBaseName)) File.Delete(DataBaseName);
            SQLiteConnection.CreateFile(DataBaseName);
            DBConnection = "Data Source=" + DataBaseName;
            ConstructTable();
        }

        protected virtual void ConstructTable()
        {
        }
        
    }
}
