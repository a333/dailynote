﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace DailyNote
{
    /// <summary>
    ///     Represents a class that manages the localization features.
    /// </summary>
    public static class LocalizationManager
    {
        private static readonly string ResourceFileNameDll = "Loc_"+ CultureInfo.CurrentCulture+".txt";

        public static bool Loaded = false;

        private static readonly Dictionary<string, string> LocaleResources = new Dictionary
            <string, string>()
        {
            {"Str_MainAchievement", "Main day achievement"},
            {"Str_Motivation", "Keep it up!"},
        };

        static LocalizationManager()
        {
            //todo turkish test on directory names
            string assemblyFile = (new Uri(Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
            string path = Path.Combine(Path.GetDirectoryName(assemblyFile), "Locales",ResourceFileNameDll);
            if(LicenseManager.UsageMode != LicenseUsageMode.Designtime)
                LoadFrom(path);
            Loaded = true;
        }

        public static void LoadFrom(string path)
        {
            Logger.Debug("Loading localization from: " + path);
            try
            {
                var lines = File.ReadAllLines(path, Encoding.UTF8);
                var name = new StringBuilder();
                var value = new StringBuilder();
                foreach (var item in lines)
                {
                    name.Clear(); 
                    value.Clear();
                    if (item.Length > 1 && item[0] == '&')
                    {
                        for (int i = 1; i < item.Length; i++)
                        {
                            if (Char.IsSeparator(item[i]))
                            {
                                value.Append(item.Substring(i+1, item.Length-i-1));
                                break;
                            }
                            name.Append(item[i]);
                        }
                        var nameStr = name.ToString();
                        if (nameStr.Length == 0)
                        {
                            Logger.Warn("Locale resource name is empty");
                            continue;
                        }
                        if (value.Length == 0)
                        {
                            Logger.Warn("Locale resource value is empty");
                            continue;
                        }
                        if (!LocaleResources.ContainsKey(nameStr))
                        {
                            Logger.Warn("Unknown locale key "+nameStr);
                            continue;
                        }
                        LocaleResources[name.ToString()] = value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Localization loading error.");
                Logger.Error(ex);
            }
        }

        public static string GetLocalisationString(string key)
        {
            return LocaleResources[key];
        }
    
    }
}