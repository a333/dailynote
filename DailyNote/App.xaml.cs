﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DailyNote.Models;
using DailyNote.ViewModels;


namespace DailyNote
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var dbHelper = new NoteDBHelper();

            var cvm = new CalendarViewModel(dbHelper);
            var cv = new CalendarViewer(cvm);
            var mvm = new MainViewModel(dbHelper,cvm);
            var mainWindow = new MainWindow(mvm, cvm,cv);
            cvm.Init(DateTime.Now);
            mainWindow.Show();
        }

      
    }
}
