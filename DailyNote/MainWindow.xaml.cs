﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DailyNote.ViewModels;

namespace DailyNote
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainViewModel VM { get; set; }
        public CalendarViewModel CVM { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public MainWindow(MainViewModel vm, CalendarViewModel cvm, UserControl calendarViewer)
        {
            VM = vm;
            CVM = cvm;
            InitializeComponent();
            cvm.NoteSelectionChanged += cvm_NoteSelectionChanged;
            ModuleContentControl.Content = calendarViewer;
            DataContext = this;
        }

        void cvm_NoteSelectionChanged(object sender, NoteEventArgs e)
        {
            VM.SetCEVM(e.VM);
        }

      
        private void CmdClose_OnClick(object sender, RoutedEventArgs e)
        {
            VM.SaveNote();
            Close();
        }

        private void CmdInsertTimeStamp(object sender, RoutedEventArgs e)
        {
            int caretIndex = txtContent.CaretIndex;
            string date = DateTime.Now.ToString("HH:mm");
            txtContent.Text = txtContent.Text.Insert(txtContent.CaretIndex, date);
            txtContent.CaretIndex = caretIndex + date.Length;
        }


        private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
                {
                    if (Keyboard.IsKeyDown(Key.X))
                    {
                        var res = MessageBox.Show(this,
                            "Are you sure you want to clear the base? This will remove all your records!", "Clearing DB",
                            MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                        if (res == MessageBoxResult.OK)
                        {
                            VM.ClearDB();

                            Close();
                        }
                    }
                }

                if (Keyboard.IsKeyDown(Key.T))
                {
                    CmdInsertTimeStamp(txtContent, new RoutedEventArgs());
                }
            }
        }
    }
}
