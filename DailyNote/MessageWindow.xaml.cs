﻿using System.Windows;
using System.Windows.Input;

namespace DailyNote
{
    public partial class MessageWindow
    {
        public MessageWindow(string message)
        {
            InitializeComponent();
            TxtData.Text = message;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ButtonState == MouseButtonState.Pressed)
                DragMove();
        }

        public static void ShowWindow(string message)
        {
            var m = new MessageWindow(message) { WindowStartupLocation = WindowStartupLocation.CenterScreen };
            m.ShowDialog();
        }
    }
}
